﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using sps.Model;

namespace sps.Controllers
{
    public class AdminController : Controller
    {
        spsEntities5 db = new spsEntities5();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult createaccount(string name,string pwd,string mail)
        {
            try
            {
           

                var u_info = new Admin_signup
                {
                    admin_name = name,
                    admin_pwd = pwd,
                    admin_email = mail,
                    

                };
                db.Admin_signup.Add(u_info);
                db.SaveChanges();
                return Json(new { success = true });
            }
            catch (Exception exception)
            {
                return Json(new { success = false, message = exception.Message });
            }
        }
        public JsonResult login(string loginemail, string loginpwd)
        {
            try
            {
 
                var selectcode = db.Admin_signup.Where(ab => ab.admin_email == loginemail && ab.admin_pwd == loginpwd).FirstOrDefault();

                if (selectcode == null)
                {
                    return Json(new { success = false });
                }
                else
                {
                    Session["u_name"] = selectcode.admin_name;
                    Session["u_id"] = selectcode.admin_no;
                    return Json(new { success = true });

                }

            }
            catch (Exception exception)
            {
                return Json(new { success = false, message = exception.Message });
            }
        }
        public ActionResult dashboard()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Addemployee()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Addemployee(FormCollection ff)
        {
            try
            {
                Add_employee emp = new Add_employee();
                emp.Emp_name = ff["empname"];
                emp.Emp_father_name = ff["empfathername"];
                emp.Emp_address = ff["empaddress"];
                emp.Emp_cnic = ff["empcnic"];
                emp.Emp_designation = ff["empdesignation"];
                emp.Emp_department = ff["empdeprtmnt"];
                emp.Emp_phone = ff["empphone"];
                db.Add_employee.Add(emp);
               // db.Add_employee.Add(emp);

                db.SaveChanges();
                ViewBag.success = "Inserted Successfully";
            }
            catch(Exception p)
            {
                ViewBag.success = "Not Inserted Successfully";
            }
                return View();
                  
        }

        public ActionResult employeedetail()
        {
            ViewBag.msg = db.Add_employee.ToList();
            return View();
        }
        public ActionResult search_employee(FormCollection ff)
        {
            var category = ff["ddlsearch"].ToString();
            var empname = ff["byname"].ToString();
            var cnin = ff["bycnic"].ToString();
            if(category=="0")
            {
                ViewBag.msg = db.Add_employee.Where(abc=>abc.Emp_name==empname).ToList();
            }
            else
            {
                ViewBag.msg = db.Add_employee.Where(abc=>abc.Emp_cnic==cnin).ToList();
            }
            return View("employeedetail");
        }
        public ActionResult edit_employee(FormCollection ff)
        {
            int id = Convert.ToInt32(ff["empid_name"]);
            var rst = db.Add_employee.Where(abc => abc.Emp_no == id).SingleOrDefault();
            rst.Emp_name = ff["txtname_name"].ToString();
            rst.Emp_father_name = ff["txtfname_name"].ToString();
            rst.Emp_address = ff["txtaddress_name"];
            rst.Emp_cnic = ff["txtcnic_name"];
            rst.Emp_designation = ff["txtdesig_name"];
            rst.Emp_department = ff["txtdept_name"];
            rst.Emp_phone = ff["txtphn_name"];
            db.SaveChanges();
            return RedirectToAction("employeedetail");
        }
        public ActionResult delete(int id)
        {
            var rst = db.Add_employee.Where(abc => abc.Emp_no == id).SingleOrDefault();
            db.Add_employee.Remove(rst);
            db.SaveChanges();
            return RedirectToAction("employeedetail");
        }
        [HttpGet]
        public ActionResult empsalarysheet()
        {
            ViewBag.vv = db.Add_employee.GroupBy(x => x.Emp_name).Select(x => x.FirstOrDefault());
            ViewBag.ss = db.Add_employee.GroupBy(y => y.Emp_department).Select(y => y.FirstOrDefault());
            ViewBag.mu = db.Add_employee.GroupBy(z => z.Emp_designation).Select(z => z.FirstOrDefault());
            return View();
        }
        [HttpPost]
        public ActionResult empsalarysheet(FormCollection u)
        {
            
            Emp_salarysheet salrysheet = new Emp_salarysheet();
            salrysheet.Emp_sal_name = u["emp_name"];
            salrysheet.Emp_sal_departmnt = u["emp_departmnt"];
            salrysheet.Emp_sal_desgntion = u["emp_designation"];
            salrysheet.Emp_gross_sal = u["gross_salary"];
            salrysheet.Emp_working_days = u["working_days"];
            salrysheet.Emp_perday_sal = u["perday_salry"];
            salrysheet.Emp_advance_sal = u["advance"];
            salrysheet.Emp_net_sal = u["net_salry"];
            salrysheet.Emp_pending_amount = u["pending_amount"];
            salrysheet.date = DateTime.Now.ToString();
            db.Emp_salarysheet.Add(salrysheet);
            db.SaveChanges();
            ViewBag.success = "Data Inserted Successfully";
       
            return RedirectToAction("empsalarysheet");
        }
        public ActionResult deleteitem(int id)
        {
            var rsst = db.Add_item.Where(ab => ab.item_no == id).SingleOrDefault();
            db.Add_item.Remove(rsst);
            db.SaveChanges();
            return RedirectToAction("Additem");
        }
        public ActionResult edititem(FormCollection kml)
        {
            int id = Convert.ToInt32(kml["edit_itemno"]);
            var rst = db.Add_item.Where(abc => abc.item_no == id).SingleOrDefault();
            rst.ite_detail = kml["edit_itemdetail"].ToString();
            rst.item_weight = kml["edit_itemweight"].ToString();
            rst.item_rate = kml["edit_itemrate"].ToString();
            rst.item_amount = kml["edit_itemamount"].ToString();
            rst.qty =Convert.ToInt32(kml["edit_itemqty"]);
            db.SaveChanges();
            return RedirectToAction("Additem");
        }
        public ActionResult viewsalary()
        {
            ViewBag.msg = db.Emp_salarysheet.ToList();
            return View();
        }
        [HttpGet]
        public ActionResult Additem()
        {
            ViewBag.itm = db.Add_item.ToList();
            return View();
        }
        [HttpPost]
        public ActionResult Additem(FormCollection fc)
        {
            
            Add_item itm = new Add_item();
            HttpPostedFileBase file = Request.Files["fleup"];
            string ImageName = Path.GetFileName(file.FileName);
            string physicalPath = Server.MapPath("~/image/" + ImageName);
            file.SaveAs(physicalPath);
            itm.ite_detail =fc["item_detail"];
            itm.item_weight = fc["item_weight"];
            itm.item_rate = fc["item_rate"];
            itm.item_amount = fc["amount"];
            itm.date = DateTime.Now.ToString();
            itm.qty =Convert.ToInt32(fc["item_qty"]);
            itm.item_img = ImageName;
            db.Add_item.Add(itm);
            db.SaveChanges();
            ViewBag.success = "Inserted Successfully";
        
            return RedirectToAction("Additem");
        }
        [HttpGet]
        public ActionResult Addstock()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult Addstock(FormCollection adds)
        {
            Add_stock stock = new Add_stock();
            stock.mandi_shop_name = adds["mandi_shop_name"];
            stock.vahicle_no = adds["vahicle_no"];
            stock.load_weight = adds["load_weight"];
            stock.unload_weight = adds["unload_weight"];
            stock.per_kg_rate = adds["per_kg_rate"];
            stock.total_amount = adds["total_amount"];
            stock.date = DateTime.Now.ToString();
            db.Add_stock.Add(stock);
            db.SaveChanges();
            ViewBag.message = "Data Inserted Successfully";
            return View();
        }
        public ActionResult deletestock(int id)
        {
            var usi = db.Add_stock.Where(ab => ab.stock_no == id).SingleOrDefault();
            db.Add_stock.Remove(usi);
            db.SaveChanges();
            return RedirectToAction("viewmandistock");
        }
        public ActionResult editstock(FormCollection edtstck)
        {
            int id = Convert.ToInt32(edtstck["empid_name"]);
            var rst = db.Add_stock.Where(abc => abc.stock_no == id).SingleOrDefault();
            rst.mandi_shop_name = edtstck["editshop_name"].ToString();
            rst.vahicle_no = edtstck["editvahicle_no"].ToString();
            rst.load_weight = edtstck["editload_weight"].ToString();
            rst.unload_weight = edtstck["editunload_weight"].ToString();
            rst.per_kg_rate = edtstck["editperkg_rate"].ToString();
            rst.total_amount = edtstck["total_amount"].ToString();
            db.SaveChanges();
            return RedirectToAction("viewmandistock");
        }

        public ActionResult viewmandistock()
        {
            ViewBag.stk = db.Add_stock.ToList();
            return View();
        }
        public ActionResult viewmonthlymandistock()
        {
            ViewBag.stk = db.Add_stock.ToList();
            return View();
        }
        public ActionResult editmonthly_stock(FormCollection mnthlystk)
        {
            int id = Convert.ToInt32(mnthlystk["empid_name"]);
            var rst = db.Add_stock.Where(abc => abc.stock_no == id).SingleOrDefault();
            rst.mandi_shop_name = mnthlystk["editshop_name"].ToString();
            rst.vahicle_no = mnthlystk["editvahicle_no"].ToString();
            rst.load_weight = mnthlystk["editload_weight"].ToString();
            rst.unload_weight = mnthlystk["editunload_weight"].ToString();
            rst.per_kg_rate = mnthlystk["editperkg_rate"].ToString();
            rst.total_amount = mnthlystk["total_amount"].ToString();
            db.SaveChanges();
            return RedirectToAction("viewmonthlymandistock");
        }
        public ActionResult deletemonthly_stock(int id)
        {
            var mushi = db.Add_stock.Where(ab => ab.stock_no == id).SingleOrDefault();
            db.Add_stock.Remove(mushi);
            db.SaveChanges();
            return RedirectToAction("viewmonthlymondistock");
        }
        [HttpGet]
        public ActionResult Addtransection()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Addtransection( FormCollection mu)
        {
            Add_transection trnsction = new Add_transection();
            trnsction.shop_weight = mu["shop_weight"];
            trnsction.balance_sheet = mu["balnce_sheet"];
            trnsction.client_amount = mu["client_amount"];
            trnsction.retail_salryamount = mu["retail_sal_amount"];
            trnsction.daily_profit = mu["daily_profit"];
            trnsction.date = DateTime.Now.ToString();
            db.Add_transection.Add(trnsction);
            db.SaveChanges();
            ViewBag.show = "Data Inserted Successfully";
            return View();
        }
        public JsonResult insert_salaryslip(string name_pass,string date_pass)
        {
            try
            {
                var month_txt= (Convert.ToDateTime(date_pass)).ToString("MM/yyyy").ToString();
                var dateget = (Convert.ToDateTime(date_pass)).ToString("MM/dd/yyyy").ToString();
                var rst = db.Emp_salarysheet.Where(ab => ab.Emp_sal_name == name_pass).SingleOrDefault();
                var chk= db.Emp_given_salaries.Where(ab => ab.Emp_sal_name == name_pass && ab.month==month_txt).SingleOrDefault();
                if (chk == null)
                {
                    Emp_given_salaries salrysheet = new Emp_given_salaries();
                    salrysheet.Emp_sal_name = rst.Emp_sal_name;
                    salrysheet.Emp_sal_departmnt = rst.Emp_sal_departmnt;
                    salrysheet.Emp_sal_desgntion = rst.Emp_sal_desgntion;
                    salrysheet.Emp_gross_sal = rst.Emp_gross_sal;
                    salrysheet.Emp_working_days = rst.Emp_working_days;
                    salrysheet.Emp_perday_sal = rst.Emp_perday_sal;
                    salrysheet.Emp_advance_sal = rst.Emp_advance_sal;
                    salrysheet.Emp_net_sal = rst.Emp_net_sal;
                    salrysheet.Emp_pending_amount = rst.Emp_pending_amount;
                    salrysheet.total_salary = rst.Emp_net_sal;
                    salrysheet.date = dateget;
                    salrysheet.month = month_txt;
                    db.Emp_given_salaries.Add(salrysheet);
                    db.SaveChanges();
                }
                else
                {
                    chk.Emp_sal_name = rst.Emp_sal_name;
                    chk.Emp_sal_departmnt = rst.Emp_sal_departmnt;
                    chk.Emp_sal_desgntion = rst.Emp_sal_desgntion;
                    chk.Emp_gross_sal = rst.Emp_gross_sal;
                    chk.Emp_working_days = rst.Emp_working_days;
                    chk.Emp_perday_sal = rst.Emp_perday_sal;
                    chk.Emp_advance_sal = rst.Emp_advance_sal;
                    chk.Emp_net_sal = rst.Emp_net_sal;
                    chk.Emp_pending_amount = rst.Emp_pending_amount;
                    chk.total_salary = rst.Emp_net_sal;
                    chk.date = dateget;
                    chk.month = month_txt;
                    db.SaveChanges();
                }
                return Json(new { success = true });
            }
            catch (Exception exception)
            {
                return Json(new { success = false, message = exception.Message});
            }
        }
        public ActionResult edit_employee_salry(FormCollection ff)
        {
            int id = Convert.ToInt32(ff["empid_name"]);
            var rst = db.Emp_salarysheet.Where(abc => abc.Emp_sal_id == id).SingleOrDefault();
            rst.Emp_sal_name = ff["txtname_name"].ToString();
            rst.Emp_sal_departmnt = ff["txtemp_dpt"].ToString();
            rst.Emp_sal_desgntion = ff["txtemp_desgntion"].ToString();
            rst.Emp_gross_sal = ff["txtemp_grossal"].ToString();
            rst.Emp_working_days = ff["txtdemp_wrkngday"].ToString();
            rst.Emp_perday_sal = ff["txtemp_perdaysal"].ToString();
            rst.Emp_advance_sal = ff["txtempadvnc_sal"].ToString();
            rst.Emp_net_sal = ff["txtemp_netsal"].ToString();
            rst.Emp_pending_amount = ff["txtemp_pendindamount"].ToString();
            db.SaveChanges();
            return RedirectToAction("viewsalary");
        }
        public ActionResult delete_emp_salary(int id)
        {
            var musi = db.Emp_salarysheet.Where(ab => ab.Emp_sal_id == id).SingleOrDefault();
            db.Emp_salarysheet.Remove(musi);
            db.SaveChanges();
            return RedirectToAction("viewsalary");
        }
        public ActionResult viewtransection()
        {
            ViewBag.trns = db.Add_transection.ToList();
            return View();
        }
        public ActionResult delete_transection(int id)
        {
            var musi = db.Add_transection.Where(ab => ab.transction_id == id).SingleOrDefault();
            db.Add_transection.Remove(musi);
            db.SaveChanges();
            return RedirectToAction("viewtransection");
        }
        public ActionResult emp_sal_histry()
        {
            ViewBag.msg = db.Emp_given_salaries.ToList();
            return View();
        }
        public ActionResult delete_emp_sal_histry(int id)
        {
            var musi = db.Emp_given_salaries.Where(ab => ab.Emp_sal_id == id).SingleOrDefault();
            db.Emp_given_salaries.Remove(musi);
            db.SaveChanges();
            return RedirectToAction("emp_sal_histry");
        }
        [HttpGet]
        public ActionResult client_orders()
        {
            ViewBag.msg = db.order_tbl.ToList();
            var rst = db.client_login.ToList();
            return View(rst);
        }
        [HttpPost]
        public ActionResult client_orders(FormCollection rr)
        {
            ViewBag.msg = db.order_tbl.ToList();
            var rst = db.client_login.ToList();
            int ft =Convert.ToInt32(rr["empid_name"].ToString());
            var oop = db.order_tbl.Where(abc => abc.order_no == ft).SingleOrDefault();
            oop.dilivery_date = rr["delivry_date"].ToString();
            oop.order_status = "Accept";
            db.SaveChanges();
            return View(rst);
        }
        public ActionResult clientordershow(string id)
        {
            int orders = Convert.ToInt32(id);
            ViewBag.lst = db.client_order.Where(ab => ab.orderno == orders).ToList();
            var rst = db.Add_item.ToList();

            return View(rst);
        }

        public ActionResult delete_client_order(int id)
        {
            var musi = db.client_order.Where(ab => ab.order_id == id).SingleOrDefault();
            db.client_order.Remove(musi);
            db.SaveChanges();
            return RedirectToAction("client_orders");
            ;
        }
    }
}