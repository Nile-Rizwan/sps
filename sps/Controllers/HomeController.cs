﻿using sps.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sps.Controllers
{
    public class HomeController : Controller
    {
        spsEntities5 db = new spsEntities5();
        // GET: Home
        [HttpGet]
        public ActionResult Index(FormCollection f)
        {
            Add_item itm = new Add_item();
            ViewBag.mu = db.Add_item.ToList().Take(5);

            return View();
        }
        [HttpPost]
        public ActionResult Index()
        {
            return View();
        }
     
        public ActionResult loginn(FormCollection ff)
        {
            
            client_login signin = new client_login();
            var mail = ff["Email"].ToString();
           var pwd  = ff["Password"].ToString();
          var rst= db.client_login.Where(ab => ab.client_email == mail && ab.client_pwd == pwd).SingleOrDefault();
            if(rst==null)
            {
                ViewBag.msg = "Invalid Email OR password";
                return RedirectToAction("login");
            }
            else
            {
                Session["u_name"] = rst.user_name;
                Session["u_id"] = rst.client_login_id;
                return RedirectToAction("checkout");
            }
            
        }
        [HttpGet]
        public ActionResult login()
        {
            Add_item itm = new Add_item();
            ViewBag.mu = db.Add_item.ToList().Take(5);
            return View();
        }
        [HttpPost]
        public ActionResult login( FormCollection logn)
        {
            client_login clintlgn = new client_login();
            clintlgn.user_name = logn["Username"];
            clintlgn.client_email = logn["Email"];
            clintlgn.client_pwd = logn["Password"];
            db.client_login.Add(clintlgn);
            db.SaveChanges();
            return RedirectToAction("login");
        }
        public ActionResult checkout()
        {
            Add_item itm = new Add_item();
            ViewBag.mu = db.Add_item.ToList().Take(5);
            return View();
        }
        [HttpGet]
        public ActionResult clientplaceorder(FormCollection m)
        {
            Add_item showitm = new Add_item();
            ViewBag.mm = db.Add_item.ToList();
            Add_item itm = new Add_item();
            ViewBag.mu = db.Add_item.ToList().Take(5);
            return View();
        }
        [HttpPost]
        public ActionResult clientplaceorder()
        {
            return View();
        }
        public JsonResult InsertValue(Add_item[] itemlist,string phn,string landmark, string town,string addresstype)
        {
            
            var rst = db.order_tbl.OrderByDescending(x => x.order_no).Select(y => y.order_no).Take(1).FirstOrDefault();
            if (rst == null)
            {
                order_tbl o = new order_tbl();
                o.order_no = 100;
                o.customerid = Convert.ToInt32(Session["u_id"]);
                o.cellno = phn;
                o.addresses = landmark;
                o.city = town;
                o.address_type = addresstype;
                o.order_status = "Pending";
                o.shippent_method = "COD";
                o.order_date = DateTime.Now.ToShortDateString();
                db.order_tbl.Add(o);
                db.SaveChanges();
                foreach (Add_item i in itemlist)
                {
                    client_order ordr = new client_order();
                    ordr.Quantity = i.qty.ToString();
                    ordr.total_price = i.item_amount;
                    ordr.client_id = Convert.ToInt32(Session["u_id"]);
                    ordr.product_id = i.item_no;
                    ordr.orderno = 100;
                    db.client_order.Add(ordr);
                    db.SaveChanges();
                }
            }
            else
            {
               var ordernumbers = db.order_tbl.OrderByDescending(x => x.order_no).Select(y => y.order_no).Take(1).FirstOrDefault();
                order_tbl o = new order_tbl();
                o.order_no=Convert.ToInt32(ordernumbers)+1;
                o.customerid = Convert.ToInt32(Session["u_id"]);
                o.cellno = phn;
                o.addresses = landmark;
                o.city = town;
                o.address_type = addresstype;
                o.order_status = "Pending";
                o.shippent_method = "COD";
                o.order_date = DateTime.Now.ToShortDateString();
                db.order_tbl.Add(o);
                db.SaveChanges();
                foreach (Add_item i in itemlist)
                {
                    client_order ordr = new client_order();
                    ordr.Quantity = i.qty.ToString();
                    ordr.total_price = i.item_amount;
                    ordr.client_id = Convert.ToInt32(Session["u_id"]);
                    ordr.product_id = i.item_no;
                    ordr.orderno = Convert.ToInt32(ordernumbers) + 1;
                    db.client_order.Add(ordr);
                    db.SaveChanges();
                }
            }
            return Json("Ok");
        }
        [HttpGet]
        public ActionResult contactus()
        {
            Add_item itm = new Add_item();
            ViewBag.mu = db.Add_item.ToList().Take(5);
            return View();
        }
        [HttpPost]
        public ActionResult contactus(FormCollection cntctus)
        {
            return View();
        }
        [HttpGet]
        public ActionResult aboutus()
        {
            Add_item itm = new Add_item();
            ViewBag.mu = db.Add_item.ToList().Take(5);
            return View();
        }
        [HttpPost]
        public ActionResult aboutus(FormCollection abtus)
        {
            return View();
        }
        public ActionResult MyOrder()
        {
           int userid= Convert.ToInt32(Session["u_id"]);
           var rst = db.client_login.Where(abc => abc.client_login_id == userid).ToList();
           ViewBag.msg = db.order_tbl.Where(abc => abc.order_status == "Accept" && abc.customerid==userid).ToList();
           return View(rst);
        }
        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Index");
        }
    }
}