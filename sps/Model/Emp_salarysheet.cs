//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace sps.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Emp_salarysheet
    {
        public int Emp_sal_id { get; set; }
        public string Emp_sal_name { get; set; }
        public string Emp_sal_departmnt { get; set; }
        public string Emp_sal_desgntion { get; set; }
        public string Emp_gross_sal { get; set; }
        public string Emp_working_days { get; set; }
        public string Emp_perday_sal { get; set; }
        public string Emp_advance_sal { get; set; }
        public string Emp_net_sal { get; set; }
        public string Emp_pending_amount { get; set; }
        public string date { get; set; }
    }
}
